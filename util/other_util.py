import hashlib
import random

def encrypt_string(hash_string):
    sha_signature = hashlib.sha256(hash_string.encode()).hexdigest()
    return sha_signature


def get_random_string(length):
    sample_letters = "abcdefghijklmnopqrstuvwxyz"
    return "".join((random.choice(sample_letters) for i in range(length)))

def get_random_number(length):
    sample_letters = "1234567890"
    return "".join((random.choice(sample_letters) for i in range(length)))

def without_keys(d, keys):
    return {x: d[x] for x in d if x not in keys}


def raise_error(msg="", param_name=""):
    import falcon
    raise falcon.HTTPBadRequest(description=msg)

def raise_forbidden(msg=''):
    import falcon
    raise falcon.HTTPForbidden(title=msg)
