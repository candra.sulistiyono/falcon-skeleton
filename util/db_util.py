from pony.orm import *
from config import config

db2 = Database()
db2.bind(
    config.driverDB,
    host=config.hostDB,
    user=config.userDB,
    passwd=config.passwordDB,
    db=config.dbName,
    charset="utf8mb4",
)
sql_debug(config.dbDebug)

