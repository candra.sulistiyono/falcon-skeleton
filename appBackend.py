import os

import falcon
from util.jwt_auth_backend import JWTAuthBackend
from util.falcon_midleware import FalconAuthMiddleware
from falcon_multipart.middleware import MultipartMiddleware


from config import config
from router.core_routes import core_routes
from router.user_routes import user_routes
from router.member_routes import member_routes
from util.jwt_util import portprq_auth

os.environ["TZ"] = "Asia/Jakarta"
os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

api = falcon.App(
    cors_enable=True,
    middleware=[
        FalconAuthMiddleware(
            JWTAuthBackend(
                portprq_auth,
                secret_key=config.secret_jwt,
                algorithm="HS512",
                required_claims=["exp", "token"],
            ),
            exempt_routes=["/docs", "/metrics"],
            exempt_methods=["OPTIONS", "HEAD"],
        ),
        MultipartMiddleware(),
    ],
)
api.req_options.auto_parse_form_urlencoded = True

print("API Backend")
core_routes(api)
user_routes(api)
member_routes(api)
