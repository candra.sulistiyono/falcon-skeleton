from entitas.user.resources import *
from entitas.echo.resources import *


def core_routes(api):
    api.add_route("/api/echo", EchoResource())
    api.add_route("/api/login", UserLoginResource())
    api.add_route("/api/user/signup", UserSignupResource())

