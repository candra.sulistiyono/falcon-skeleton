from entitas.user.resources import *
from entitas.status.resources import *

def user_routes(api):
    api.add_route("/api/user/password", UserUpdatePasswordWithResource())
    api.add_route("/api/user/profile", UserUpdateProfileWithIdResource())
    api.add_route("/api/user/logout", UserLogoutWithIdResource())
    api.add_route("/api/user/status", UserStatusResource())
    api.add_route("/api/user/status/{status_id}", UserStatusWithIdResource())
