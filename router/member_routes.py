from entitas.user.resources import *


def member_routes(api):
    api.add_route("/api/member/password", MemberUpdatePasswordWithResource())
    api.add_route("/api/member/profile", MemberUpdateProfileWithIdResource())
    api.add_route("/api/member/logout", MemberLogoutWithIdResource())
