import os
userDB = os.environ["userDB"]
passwordDB = os.environ["passwordDB"]
hostDB = os.environ["hostDB"]
portDB = int(os.environ["portDB"])
dbName = os.environ["dbName"]
driverDB = os.environ["driverDB"]
dbDebug = False
if os.environ["dbDebug"].strip().upper() == "TRUE":
    dbDebug = True
secret_jwt = os.environ["secret_jwt"]
TYPE_TOKEN_USER = 'user'
