from datetime import date, datetime
from pony.orm import *
from entitas.status.models import Status
from entitas.user.models import User
from util.db_util import db2


class UserDB(db2.Entity):
    _table_ = "user"
    id = PrimaryKey(int, auto=True)
    name = Optional(str, nullable=True)
    address = Optional(str, nullable=True)
    hp = Optional(str, nullable=True)
    email = Optional(str, nullable=True)
    password = Optional(str, nullable=True)
    token = Optional(str, nullable=True)
    last_login = Optional(datetime, nullable=True)
    birth_date = Optional(date, nullable=True)
    birth_place = Optional(str, nullable=True)
    firebase_token = Optional(str, nullable=True)
    ws_id = Optional(str, nullable=True)
    active = Optional(int, nullable=True)
    picture = Optional(str, 1000, nullable=True)
    role = Optional(str, nullable=True)
    description = Optional(str, 100000, nullable=True)
    tag = Optional(str, nullable=True)
    organization_name = Optional(str, nullable=True)
    organization_address = Optional(str, nullable=True)
    parent_user_id = Optional(int, nullable=True)
    created_date = Optional(datetime, nullable=True)
    updated_date = Optional(datetime, nullable=True)

    def to_model(self):
        item = User()
        item.id = self.id
        item.name = self.name
        item.address = self.address
        item.hp = self.hp
        item.email = self.email
        item.password = self.password
        item.token = self.token
        item.last_login = self.last_login
        item.birth_date = self.birth_date
        item.birth_place = self.birth_place
        item.firebase_token = self.firebase_token
        item.ws_id = self.ws_id
        item.active = self.active
        item.picture = self.picture
        item.role = self.role
        item.description = self.description
        item.tag = self.tag
        item.organization_name = self.organization_name
        item.organization_address = self.organization_address
        item.parent_user_id = self.parent_user_id
        item.created_date = self.created_date
        item.updated_date = self.updated_date
        return item

class StatusDB(db2.Entity):
    _table_ = "status"
    id = PrimaryKey(int, auto=True)
    name = Optional(str, nullable=True)
    description = Optional(str, nullable=True)
    created_date = Optional(datetime, nullable=True)
    updated_date = Optional(datetime, nullable=True)

    def to_model(self):
        item = Status()
        item.id = self.id
        item.name = self.name
        item.description = self.description
        item.created_date = self.created_date
        item.updated_date = self.updated_date
        return item

if db2.schema is None:
    db2.generate_mapping(create_tables=False)


