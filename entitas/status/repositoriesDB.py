from pony.orm import *

from database.schema import StatusDB


@db_session
def get_all(to_model=False):
    result = []
    try:
        for item in select(s for s in StatusDB):
            if to_model:
                result.append(item.to_model())
            else:
                result.append(item.to_model().to_response())

    except Exception as e:
        print("error StatusDB getAll: ", e)
    return result


@db_session
def get_all_with_pagination(page=1, limit=9, filters=[], to_model=False):
    result = []
    total_record = 0
    try:
        data_in_db = select(s for s in StatusDB).order_by(desc(StatusDB.id))
        for item in filters:
            if item["field"] == "id":
                data_in_db = data_in_db.filter(id=item["value"])
            elif item["field"] == "name":
                data_in_db = data_in_db.filter(lambda d: item["value"] in d.name)

        total_record = data_in_db.count()
        if limit > 0:
            data_in_db = data_in_db.page(pagenum=page, pagesize=limit)
        else:
            data_in_db = data_in_db
        for item in data_in_db:
            if to_model:
                result.append(item.to_model())
            else:
                result.append(item.to_model().to_response())

    except Exception as e:
        print("error StatusDB getAllWithPagination: ", e)
    return result, {
        "total": total_record,
        "page": page,
        "total_page": (total_record + limit - 1) // limit if limit > 0 else 1,
    }


@db_session
def find_by_id(id=None):
    data_in_db = select(s for s in StatusDB if s.id == id)
    if data_in_db.first() is None:
        return None
    return data_in_db.first().to_model()


@db_session
def find_by_name(name="", to_model=False):
    try:
        if to_model:
            return StatusDB.get(name=name).to_model()
        else:
            return StatusDB.get(name=name).to_model().to_response()
    except Exception as e:
        print("error StatusDB findByName: ", e)
        return None


@db_session
def update(id=0, json_object={}, to_model=False):
    try:
        updated_status = StatusDB[id]
        updated_status.name = json_object["name"]
        updated_status.description = json_object["description"]
        commit()
        if to_model:
            updated_status.to_model()
        else:
            return updated_status.to_model().to_response()
    except Exception as e:
        print("error Status update: ", e)
        return None


@db_session
def delete_by_id(id=None):
    try:
        StatusDB[id].delete()
        commit()
        return True
    except Exception as e:
        print("error Status deleteById: ", e)
    return


@db_session
def insert(json_object={}, to_model=False):
    try:
        new_status = StatusDB(
            name=json_object["name"],
            description=json_object["description"]
        )
        commit()
        if to_model:
            return new_status.to_model()
        else:
            return new_status.to_model().to_response()
    except Exception as e:
        print("error Status insert: ", e)
        return None
