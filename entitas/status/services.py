from entitas.status import repositoriesDB
from util.other_util import  raise_error

def get_status_db_with_pagination(page=1, limit=9, filters=[], to_model=False):
    return repositoriesDB.get_all_with_pagination(
        page=page, limit=limit, filters=filters, to_model=to_model
    )


def find_status_db_by_id(id=0, to_model=False):
    status = repositoriesDB.find_by_id(id=id)
    if status is None:
        raise_error('Status not found')
    if to_model:
        return status
    return status.to_response()

def find_status_db_by_name(name="", to_model=False):
    return repositoriesDB.find_by_name(name=name, to_model=to_model)


def get_all_status_db(to_model=False):
    return repositoriesDB.get_all(to_model=to_model)


def update_status_db(id=0, json_object={}):
    status = repositoriesDB.find_by_id(id=id)
    if status is None:
        raise_error('Status not found')
    return repositoriesDB.update(id=id, json_object=json_object)


def insert_status_db(json_object={}):
    data = repositoriesDB.insert(json_object=json_object)
    return data


def delete_status_by_id(id=0):
    status = repositoriesDB.find_by_id(id=id)
    if status is None:
        raise_error('Status not found')
    return repositoriesDB.delete_by_id(id=id)
