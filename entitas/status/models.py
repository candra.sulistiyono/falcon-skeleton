class Status:
    def __init__(self, id=0, name=0, description='', created_date=None, updated_date=None):
        self.id = id
        self.name = name
        self.description = description
        self.created_date = created_date
        self.updated_date = updated_date

    def to_json(self):
        return {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "created_date": str(self.created_date),
            "updated_date": str(self.updated_date),
        }

    def to_response(self):
        return {
            "id": self.id,
            "name": self.name,
            "description": self.description
        }
