import uuid

import falcon

from entitas.user import repositoriesDB
from util.constant import EMAIL_MUST_FILL, PASSWORD_MUST_FILL
from util.jwt_util import  check_valid_email
from util.other_util import encrypt_string, raise_error, raise_forbidden
import datetime
from config.config import TYPE_TOKEN_USER


def get_user_db_with_pagination(
        page=1, limit=9, name="", to_model=False, filters=[], to_response="to_response"
):
    return repositoriesDB.get_all_with_pagination(
        page=page,
        limit=limit,
        name=name,
        to_model=to_model,
        filters=filters,
        to_response=to_response,
    )


def find_user_db_by_id(id=0, to_model=False):
    account = repositoriesDB.find_by_id(id=id)
    if account is None:
        return None
    if to_model:
        return account
    return account.to_response()


def find_user_db_by_list_id(list_id=[], to_model=False):
    accounts = repositoriesDB.find_by_list_id(list_id=list_id)
    if to_model:
        return accounts
    result = []
    for account in accounts:
        result.append(account.to_response())
    return result


def find_user_db_by_name(name="", to_model=False):
    return repositoriesDB.find_by_name(name=name, to_model=to_model)


def get_all_user_db(filters=[], to_model=False):
    return repositoriesDB.get_all(filters=filters, to_model=to_model)


def update_user_db(json_object={}):
    return repositoriesDB.update(json_object=json_object)


def update_password_user_db(json_object={}):
    from util.constant import AccountConstant

    if json_object["new_password"] == json_object["old_password"]:
        raise_error(AccountConstant.MESSAGE_NEW_PASSWORD_DOESNT_MATCH)
    if "confirm_new_password" in json_object:
        if json_object["confirm_new_password"] != json_object["new_password"]:
            raise_error(AccountConstant.MESSAGE_NEW_PASSWORD_DOESNT_MATCH)
    account = repositoriesDB.find_by_id(id=json_object["user"]["id"])
    if account is None:
        raise_error(AccountConstant.ACCOUNT_NOT_MATCH)
    if encrypt_string(json_object["old_password"]) != account.password:
        raise_error(AccountConstant.PASSWORD_NOT_MATCH)

    if "confirm_new_password" not in json_object:
        raise_error(AccountConstant.MESSAGE_NEW_PASSWORD_DOESNT_MATCH)
    json_object["id"] = account.id
    return repositoriesDB.update_password(json_object=json_object)


def insert_user_db(json_object={}, to_model=False):
    if "picture" not in json_object:
        json_object["picture"] = ''
    data, status = repositoriesDB.insert(json_object=json_object, to_model=to_model)
    return data, status


def signup_user_db(json_object={}):
    if "email" not in json_object:
        raise_error(str=EMAIL_MUST_FILL)
    if "password" not in json_object:
        raise_error(str=PASSWORD_MUST_FILL)
    if not check_valid_email(email=json_object["email"]):
        raise_error(msg='Email tidak valid')
    json_object["role"] = 'user'
    json_object["token"] = str(uuid.uuid4())
    existing_account = repositoriesDB.find_by_email(email=json_object["email"], to_model=True)

    if existing_account is not None:
        raise_error(msg='Email sudah terdaftar')

    json_object["new_password"] = json_object["password"]
    json_object['parent_user_id'] = 0
    return repositoriesDB.signup(json_object=json_object)

def delete_user_by_id(id=0):
    return repositoriesDB.delete_by_id(id=id)


def login_db(json_object={}, domain=""):
    from util.jwt_util import jwt_encode

    account_info = repositoriesDB.post_login(json_object=json_object)
    if account_info is None:
        raise_forbidden('Email atau password tidak sesuai')

    if account_info.active == 0:
        raise_error("Email belum di aktivasi")
    domain_result = ""

    account = account_info.to_response_login()
    account["domain"] = domain_result
    return jwt_encode(account, TYPE_TOKEN_USER)

def generate_jwt(id=0):
    from util.jwt_util import jwt_encode

    account_info = repositoriesDB.find_by_id(id=id)
    if account_info is None:
        raise_forbidden('Account tidak ditemukan')

    account = account_info.to_response_jwt()
    return jwt_encode(account, TYPE_TOKEN_USER, days=5000)

def get_whatsapp_token(id=0):
    account_info = repositoriesDB.find_by_id(id=id)
    if account_info is None:
        raise_forbidden('Account tidak ditemukan')

    return account_info.whatsapp_token


def find_user_db_by_token(token="", to_model=False):
    return repositoriesDB.find_by_token(token=token, to_model=to_model)


def update_profile_id_user_db(json_object={}):
    if json_object is None:
        raise_error('payload data is empty')
    account = find_user_db_by_id(id=json_object["id"], to_model=True)
    if account is None:
        raise_error(msg="akun tidak ditemukan")
    return repositoriesDB.update_profile(json_object=json_object, to_model=False)


def update_profile_id_user_db_admin(json_object={}):
    account = find_user_db_by_id(id=json_object["id"], to_model=True)
    if account is None:
        return None, "akun tidak ditemukan"
    return repositoriesDB.update_profile(json_object=json_object, to_model=False)


def logout_user_db(json_object={}):
    return repositoriesDB.reset_token_by_token(token=json_object['token'])


def get_profile_id_user_db(json_object={}):
    account = repositoriesDB.find_by_id(id=json_object["id"])
    if account is None:
        return
    return account.to_response_profile()



def get_profile_id_user_db_admin(id=0):
    account = repositoriesDB.find_by_id(id=id)
    if account is None:
        return None, ""
    return account.to_response_profile(), "success"


def find_user_db_by_email(email="", to_model=False):
    return repositoriesDB.find_by_email(email=email, to_model=to_model)


def is_email_user_exist(email=""):
    return repositoriesDB.is_email_user_exist(email=email)

def activate_user_by_email(email=""):
    return repositoriesDB.activate_user_by_email(
        email=email, active=1
    ).to_response_login()


def force_update_password_user(json_object={}):
    return repositoriesDB.update_password(json_object=json_object)

def is_token_valid(id=0, token=""):
    return repositoriesDB.is_email_user_exist(id=id, token=token)


def is_email_has_user(email=""):
    return repositoriesDB.is_email_has_user(email=email)


def refresh_token_authorization(authorization=None):
    authorization = authorization.split(' ')[1]
    import jwt
    from config.config import secret_jwt
    from util.jwt_util import jwt_encode
    try:
        if authorization is None or authorization == '':
            raise Exception('authorization empty')
        auth = jwt.decode(
            authorization, secret_jwt, algorithms="HS512"
        )
    except:
        raise falcon.HTTPUnauthorized(
            title="401 Unauthorized",
            description="Signature has expired",
            challenges=None)

    date_time_exp = datetime.datetime.fromtimestamp(auth['exp'])
    if date_time_exp <= datetime.datetime.now():
        raise falcon.HTTPUnauthorized(
            title="401 Unauthorized",
            description="Signature has expired",
            challenges=None)
    return jwt_encode(user_info=auth, type_token=TYPE_TOKEN_USER)
