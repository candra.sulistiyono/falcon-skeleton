from entitas.user import services
from util.entitas_util import generate_filters_resource, resouce_response_api


class UserLoginResource:
    auth = {"auth_disabled": True}
    def on_post(self, req, resp):
        body = req.media
        domain = ""
        if "ORIGIN" in req.headers:
            domain = req.headers["ORIGIN"]
        resouce_response_api(resp=resp, data=services.login_db(
            json_object=body, domain=domain
        ), pagination={})


class UserSignupResource:
    auth = {"auth_disabled": True}

    def on_get(self, req, resp):
        page = int(req.get_param("page", required=False, default=1))
        limit = int(req.get_param("limit", required=False, default=9))
        filters = generate_filters_resource(req=req, params_string=['name'])
        data, pagination = services.get_user_db_with_pagination(
            page=page, limit=limit, filters=filters
        )
        resouce_response_api(resp=resp, data=data, pagination=pagination)

    def on_post(self, req, resp):
        resouce_response_api(resp=resp, data=services.signup_user_db(json_object=req.media))


class UserUpdatePasswordWithResource:
    def on_put(self, req, resp):
        body = req.media
        body["user"] = req.context["user"]
        resouce_response_api(resp=resp, data=services.update_password_user_db(
            json_object=body
        ))

class MemberUpdatePasswordWithResource:
    def on_put(self, req, resp):
        body = req.media
        body["user"] = req.context["user"]
        resouce_response_api(resp=resp, data=services.update_password_user_db(
            json_object=body
        ))

class AgentUpdatePasswordWithResource:
    def on_put(self, req, resp):
        body = req.media
        body["user"] = req.context["user"]
        resouce_response_api(resp=resp, data=services.update_password_user_db(
            json_object=body
        ))

class UserUpdateProfileWithIdResource:
    def on_put(self, req, resp):
        body = req.media
        body["id"] = req.context["user"]["id"]
        resouce_response_api(resp=resp, data=services.update_profile_id_user_db(
            json_object=body
        ))

    def on_get(self, req, resp):
        resouce_response_api(resp=resp, data=services.get_profile_id_user_db(
            json_object={"id": req.context["user"]["id"]}
        ))

class MemberUpdateProfileWithIdResource:
    def on_put(self, req, resp):
        body = req.media
        body["id"] = req.context["user"]["id"]
        resouce_response_api(resp=resp, data=services.update_profile_id_user_db(
            json_object=body
        ))

    def on_get(self, req, resp):
        resouce_response_api(resp=resp, data=services.get_profile_id_user_db(
            json_object={"id": req.context["user"]["id"]}
        ))

class UserLogoutWithIdResource:
    
    def on_post(self, req, resp):
        resouce_response_api(resp=resp, data=services.logout_user_db(
            json_object={"token": req.context["user"]["token"]}
        ))

class MemberLogoutWithIdResource:
    def on_post(self, req, resp):
        resouce_response_api(resp=resp, data=services.logout_user_db(
            json_object={"token": req.context["user"]["token"]}
        ))
